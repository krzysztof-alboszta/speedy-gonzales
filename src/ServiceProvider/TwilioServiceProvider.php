<?php
declare(strict_types=1);

namespace Speedy\ServiceProvider;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Twilio\Rest\Client;

class TwilioServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['twilio'] = function ($app) {

            if (!isset($app['twilio.sid']) || !isset($app['twilio.token'])) {
                throw new \RuntimeException('Twilio configuration is missing. Provide proper SID and Token');
            }
            $sid = $app['twilio.sid'];
            $token = $app['twilio.token'];

            return new Client($sid, $token);
        };
    }

}