<?php
declare(strict_types=1);

use Silex\Application;
use Speedy\Http;
use Speedy\Listener;
use Speedy\Service\Benchmark;
use Speedy\ServiceProvider;
use Speedy\SMS;

$app = new Application();
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new SilexGuzzle\GuzzleServiceProvider(), [
    'guzzle.timeout' => 30,
]);
$app->register(new ServiceProvider\TwilioServiceProvider());

$app['event_dispatcher'] = function ($app) {
    $dispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();

    $emailListener = $app['listener.email_notifier'];
    $twilioListener = $app['listener.twilio_notifier'];

    $dispatcher->addListener('notify.slower', [$emailListener, 'onTargetSlower']);
    $dispatcher->addListener('notify.twice_slower', [$twilioListener, 'onTargetTwiceSlower']);

    return $dispatcher;
};

$app['listener.twilio_notifier'] = function ($app) {
    $clientService = $app['notification_client'];
    $smsClient = $app[$clientService];

    $senderNumber = $app['notification.sms_sender'];
    $recipientNumber = $app['notification.sms_recipient'];

    $listener = new Listener\SmsNotifier($smsClient, $senderNumber, $recipientNumber);

    return $listener;
};

$app['listener.email_notifier'] = function ($app) {
    $mailer = $app['mailer'];
    $from = $app['notification.email_sender'];
    $to = $app['notification.email_recipient'];

    $listener = new Listener\EmailNotifier($mailer, $from, $to);

    return $listener;
};

$app['sms.client.twilio'] = function ($app) {
    return new SMS\Client\TwilioClient($app['twilio']);
};

$app['sms.client.dummy'] = function ($app) {
    return new SMS\Client\DummyClient($app['logger']);
};

$app['service.measurement.latency'] = function ($app) {
    $client = new Http\Client\GuzzleClient($app['guzzle']);
    $profiler = new Benchmark\Profiler\StopWatchProfiler();

    return new Benchmark\Measurement\Latency($client, $profiler);
};
$app['service.benchmark.latency'] = function ($app) {
    $measurement = $app['service.measurement.latency'];

    return new Benchmark\Benchmark($measurement);
};

return $app;