<?php
declare(strict_types=1);

namespace Speedy\Service\Report;

interface ReportInterface
{

    public function getHeader(): array;

    public function getData(): array;

    public function getNotes(): array;

    public function getTitle(): string;

}