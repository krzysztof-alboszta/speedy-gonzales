<?php
declare(strict_types=1);

namespace Speedy\Service\Report;

interface ReportWriterInterface
{

    public function write(ReportInterface $report);
}