<?php
declare(strict_types=1);

namespace Speedy\Service\Report\Writer;


use Speedy\Service\Report\ReportInterface;
use Speedy\Service\Report\ReportWriterInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConsoleWriter implements ReportWriterInterface
{
    /** @var  SymfonyStyle */
    private $io;

    public function setInputOutput(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    public function write(ReportInterface $data)
    {
        if (!$this->io instanceof SymfonyStyle) {
            throw new \RuntimeException('Console io has not been set');
        }

        $this->io->section($data->getTitle());
        $this->io->table($data->getHeader(), $data->getData());
        foreach ($data->getNotes() as $note) {
            $this->io->note($note);
        }
    }
}