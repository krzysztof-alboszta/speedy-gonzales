<?php
declare(strict_types=1);

namespace Speedy\Service\Report\Writer;

use Speedy\Service\Report\ReportInterface;
use Speedy\Service\Report\ReportWriterInterface;

class FileWriter implements ReportWriterInterface
{
    const LOG_FILE = 'log.txt';

    public function write(ReportInterface $report)
    {
        $file = \fopen(self::LOG_FILE, 'w+');
        \fwrite($file, \sprintf(' ** %s **', $report->getTitle()) . PHP_EOL);
        $header = \implode(' | ', $report->getHeader());
        \fwrite($file, \sprintf('|| %s ||', $header) . PHP_EOL);
        foreach ($report->getData() as $row) {
            $line = \implode(' | ', $row);
            \fwrite($file, \sprintf('| %s |', $line) . PHP_EOL);
        }
        fwrite($file, PHP_EOL);
        foreach ($report->getNotes() as $note) {
            \fwrite($file, \sprintf('* %s', $note) . PHP_EOL);
        }

        \fclose($file);
    }
}