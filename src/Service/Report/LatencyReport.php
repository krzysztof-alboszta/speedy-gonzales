<?php
declare(strict_types=1);

namespace Speedy\Service\Report;

class LatencyReport implements ReportInterface
{
    const HEADER_COLUMNS = ['Index', 'Site Url', 'Time [ms]'];
    const TITLE_FORMAT = 'Latency benchmark for site %s';

    /** @var  string */
    protected $target;

    /** @var  array */
    protected $data;

    /** @var  string[] */
    protected $notes = [];

    /**
     * LatencyReport constructor.
     * @param string $target
     */
    public function __construct(string $target)
    {
        $this->target = $target;
    }

    public function getHeader(): array
    {
        return self::HEADER_COLUMNS;
    }

    public function getTitle(): string
    {
        return \sprintf(self::TITLE_FORMAT, $this->target);
    }

    /**
     * @return array [[index,url,time], ... ]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string[]
     */
    public function getNotes(): array
    {
        return $this->notes;
    }

    /**
     * @param string $note
     */
    public function addNote(string $note)
    {
        $this->notes[] = $note;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }


}