<?php
declare(strict_types=1);

namespace Speedy\Service\Report;

use Speedy\Service\Benchmark\Result\Result;

class LatencyReportFactory
{
    public static function createFromBenchmarkResult(Result $result): LatencyReport
    {
        $data = self::prepareResultData($result);
        $report = new LatencyReport($result->getTarget());
        $report->setData($data);
        $dt = new \DateTime();
        $report->addNote(\sprintf('Report performed at %s', $dt->format('Y-m-d H:i')));

        return $report;
    }

    /**
     * @param Result $result
     * @return array
     */
    protected static function prepareResultData(Result $result): array
    {
        $data = [];
        $index = 1;
        foreach ($result->getResults() as $reference => $value) {
            $data[] = [$index, $reference, $value];
            $index++;
        }

        return $data;
    }
}