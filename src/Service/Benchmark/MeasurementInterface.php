<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark;

/**
 * Interface MeasurementInterface
 *
 * Common interface for Benchmark measurement methods
 */
interface MeasurementInterface
{
    public function measure(string $target): int;

    public function getUnit(): string;

    public function getName(): string;
}