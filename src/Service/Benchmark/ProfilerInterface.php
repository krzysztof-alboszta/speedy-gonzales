<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark;


interface ProfilerInterface
{

    public function start();

    public function stop();

    public function getTime();
}