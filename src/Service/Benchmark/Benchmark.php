<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark;

use Speedy\Service\Benchmark\MeasurementInterface;
use Speedy\Service\Benchmark\Result\Result;

/**
 * Service to perform single benchmark by chosen method
 * It could be refactored to use chain of measurement methods to be perform on target and competitor URLs
 *
 * @package Speedy\Service
 */
class Benchmark
{

    /** @var  MeasurementInterface */
    private $measurement;

    /**
     * Benchmark constructor.
     *
     * @param MeasurementInterface $measurement
     */
    public function __construct(MeasurementInterface $measurement)
    {
        $this->measurement = $measurement;
    }

    /**
     * @param string   $url
     * @param string[] $referrers
     *
     * @return Result
     */
    public function testAgainstCompetitors(string $url, array $referrers): Result
    {
        $targetValue = $this->measurement->measure($url);

        $referenceValues = [];
        $referenceValues[$url] = $targetValue;

        foreach ($referrers as $referrer) {
            $referenceValues[$referrer] = $this->measurement->measure($referrer);
        }

        asort($referenceValues);

        $index = \array_search($targetValue, array_values($referenceValues), true);
        $result = new Result($url);
        $result->setTargetValue($targetValue)
            ->setTargetIndex($index)
            ->setResults($referenceValues);

        return $result;
    }
}