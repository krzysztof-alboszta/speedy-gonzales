<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark\Measurement;

use Speedy\Http\ClientInterface;
use Speedy\Service\Benchmark\MeasurementInterface;
use Speedy\Service\Benchmark\ProfilerInterface;

/**
 * Latency Measurement method
 * Perform a request to targeted URL and measure its response time
 */
class Latency implements MeasurementInterface
{

    /** @var  ClientInterface */
    private $httpClient;

    /** @var  ProfilerInterface */
    private $profiler;

    /**
     * Latency constructor.
     *
     * @param ClientInterface   $httpClient
     * @param ProfilerInterface $profiler
     */
    public function __construct(ClientInterface $httpClient, ProfilerInterface $profiler)
    {
        $this->httpClient = $httpClient;
        $this->profiler = $profiler;
    }

    public function measure(string $target): int
    {
        $this->profiler->start();
        $this->httpClient->doRequest($target);
        $this->profiler->stop();

        return $this->profiler->getTime();
    }

    public function getUnit(): string
    {
        return 's';
    }

    public function getName(): string
    {
        return 'latency';
    }

}