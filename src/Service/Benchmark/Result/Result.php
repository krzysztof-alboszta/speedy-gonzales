<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark\Result;


class Result
{

    /** @var  string */
    private $target;

    /** @var  int */
    private $targetValue;

    /** @var  int */
    private $targetIndex;

    /** @var  array */
    private $results = [];

    /**
     * Result constructor.
     * @param string $target
     */
    public function __construct($target)
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @return int
     */
    public function getTargetValue(): int
    {
        return $this->targetValue;
    }

    /**
     * @param int $targetValue
     * @return Result
     */
    public function setTargetValue(int $targetValue): Result
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * @return int order number in ranking (number of competitors that are better)
     */
    public function getTargetIndex(): int
    {
        return $this->targetIndex;
    }

    /**
     * @param int $targetIndex
     * @return Result
     */
    public function setTargetIndex(int $targetIndex): Result
    {
        $this->targetIndex = $targetIndex;

        return $this;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param array $results
     * @return Result
     */
    public function setResults(array $results): Result
    {
        $this->results = $results;

        return $this;
    }



}