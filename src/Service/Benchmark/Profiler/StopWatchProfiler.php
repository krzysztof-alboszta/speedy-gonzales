<?php
declare(strict_types=1);

namespace Speedy\Service\Benchmark\Profiler;


use Speedy\Service\Benchmark\ProfilerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class StopWatchProfiler implements ProfilerInterface
{

    /** @var  Stopwatch */
    private $stopWatch;

    public function start()
    {
        $this->stopWatch = new Stopwatch();
        $this->stopWatch->start('profile');
    }

    public function stop()
    {
        if (null === $this->stopWatch) {
            throw new \RuntimeException('Profiler has not been started yet');
        }

        $this->stopWatch->stop('profile');
    }

    public function getTime(): int
    {
        $event = $this->stopWatch->getEvent('profile');

        return $event->getDuration();
    }


}