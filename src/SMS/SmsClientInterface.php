<?php
declare(strict_types=1);

namespace Speedy\SMS;

interface SmsClientInterface
{

    public function sendMessage(string $recipient, string $body, string $from);
}