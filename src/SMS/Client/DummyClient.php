<?php
declare(strict_types=1);

namespace Speedy\SMS\Client;


use Psr\Log\LoggerInterface;
use Speedy\SMS\SmsClientInterface;

class DummyClient implements SmsClientInterface
{
    /** @var  LoggerInterface */
    protected $logger;

    /**
     * DummyClient constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function sendMessage(string $recipient, string $body, string $from)
    {
        $this->logger->info('Sending sms message', [
            'message' => $body,
            'recipient' => $recipient,
            'from' => $from,
        ]);
    }

}