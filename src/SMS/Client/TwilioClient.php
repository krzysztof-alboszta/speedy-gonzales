<?php

namespace Speedy\SMS\Client;


use Speedy\SMS\SmsClientInterface;

class TwilioClient implements SmsClientInterface
{

    /** @var  TwilioClient */
    protected $twilio;

    /**
     * TwilioClient constructor.
     *
     * @param TwilioClient $twilio
     */
    public function __construct(TwilioClient $twilio)
    {
        $this->twilio = $twilio;
    }

    public function sendMessage(string $recipient, string $body, string $from)
    {
        $this->client->messages->create(
            $recipient,
            [
                'from' => $from,
                'body' => $body,
            ]
        );
    }


}