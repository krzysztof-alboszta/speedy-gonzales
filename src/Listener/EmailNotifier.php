<?php
declare(strict_types=1);

namespace Speedy\Listener;

use Symfony\Component\EventDispatcher\Event;

class EmailNotifier
{

    /** @var  \Swift_Mailer */
    private $mailer;

    /**
     * @todo refactor to configuration object
     * @var  string
     */
    private $from;

    /**
     * @todo refactor to configuration object
     * @var  string
     */
    private $to;

    /**
     * EmailNotifier constructor.
     * @param \Swift_Mailer $mailer
     * @param string $from
     * @param string $to
     */
    public function __construct(\Swift_Mailer $mailer, $from, $to)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @todo introduce custom event with report object to add note
     * @param Event $event
     */
    public function onTargetSlower(Event $event)
    {
        $this->sendMessage(
            'Benchmark result',
            'The website is loaded slower than at least one of the competitors'
        );
    }

    protected function sendMessage(string $subject, string $body)
    {
        $message = new \Swift_Message($subject);
        $message
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }

}