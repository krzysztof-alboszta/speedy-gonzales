<?php
declare(strict_types=1);

namespace Speedy\Listener;

use Speedy\SMS\SmsClientInterface;
use Symfony\Component\EventDispatcher\Event;

class SmsNotifier
{
    /**
     * @var SmsClientInterface
     */
    private $client;

    /** @var  string */
    private $from;

    /** @var  string */
    private $to;

    /**
     * TwilioNotifier constructor.
     *
     * @param SmsClientInterface $client
     * @param string             $from
     * @param string             $to
     */
    public function __construct(SmsClientInterface $client, string $from, string $to)
    {
        $this->client = $client;
        $this->from = $from;
        $this->to = $to;
    }

    public function onTargetTwiceSlower(Event $event)
    {
        $message = 'The website is loaded twice as slow as at least one of the competitors';

        $this->client->sendMessage($this->to, $message, $this->from);
    }
}