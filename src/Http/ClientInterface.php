<?php
declare(strict_types=1);

namespace Speedy\Http;

interface ClientInterface
{
    /**
     * @param string $url
     */
    public function doRequest(string $url);
}