<?php
declare(strict_types=1);

namespace Speedy\Http\Client;

use Speedy\Http\ClientInterface;

class DummyClient implements ClientInterface
{
    const LATENCY_MIN = 100;

    const LATENCY_MAX = 10000;

    public function doRequest(string $url)
    {
        \usleep(\random_int(self::LATENCY_MIN, self::LATENCY_MAX));
    }

}