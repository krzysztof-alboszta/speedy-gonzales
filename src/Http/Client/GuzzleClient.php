<?php
declare(strict_types=1);

namespace Speedy\Http\Client;

use GuzzleHttp\Client;
use Speedy\Http\ClientInterface;

class GuzzleClient implements ClientInterface
{
    /**
     * @var Client
     */
    protected $guzzle;

    /**
     * GuzzleClient constructor.
     *
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param string $url
     */
    public function doRequest(string $url)
    {
        $this->guzzle->request('GET', $url);
    }
}