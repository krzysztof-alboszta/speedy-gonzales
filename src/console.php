<?php
declare(strict_types=1);

use Speedy\Service\Report;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;

$console = new Application('Speedy Gonzales - Silex Application', '0.1');

$console->getDefinition()->addOption(new InputOption(
    '--env',
    '-e',
    InputOption::VALUE_REQUIRED,
    'The Environment name.', 'dev'
));

$console->setDispatcher($app['dispatcher']);
$console
    ->register('benchmark')
    ->setDefinition([
        new InputArgument('website-url', InputArgument::REQUIRED, 'Benchmarked website URL'),
        new InputArgument('other-urls', InputArgument::REQUIRED, 'Competitors websites URL [comma separated]'),
    ])
    ->setDescription(<<<'TAG'
Benchmark loading time of the website in comparison to the other websites 
             (check how fast is the website's loading time in comparison to other competitors
TAG
    )
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

        $io = new SymfonyStyle($input, $output);
        $consoleWriter = new Report\Writer\ConsoleWriter();
        $consoleWriter->setInputOutput($io);

        /** @var \Speedy\Service\Report\ReportWriterInterface[] $outputWriters */
        $outputWriters = [
            $consoleWriter,
            new Report\Writer\FileWriter(),
        ];

        $target = $input->getArgument('website-url');
        $competitors = \explode(',', $input->getArgument('other-urls'));

        /** @var \Speedy\Service\Benchmark\Benchmark $benchmarkService */
        $benchmarkService = $app['service.benchmark.latency'];
        $result = $benchmarkService->testAgainstCompetitors($target, $competitors);

        $report = Report\LatencyReportFactory::createFromBenchmarkResult($result);

        // @todo move this to some ReportValidator service
        /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
        $dispatcher = $app['event_dispatcher'];
        if (0 < $result->getTargetIndex()) {
            $dispatcher->dispatch('notify.slower');
            $report->addNote('The website is loaded slower than at least one of the competitors');

            $resultTimes = array_values($result->getResults());
            if ($resultTimes[0] < 2 * $resultTimes[$result->getTargetIndex()]) {
                $dispatcher->dispatch('notify.twice_slower');
                $report->addNote('The website is loaded twice as slow than at least one of the competitors');

            }
        }

        foreach ($outputWriters as $writer) {
            $writer->write($report);
        }

    });

return $console;