<?php
// send mails immediately
$app['swiftmailer.use_spool'] = false;

// use twilio or dummy client
$app['notification_client'] = 'sms.client.twilio';

// twilio account configuration
$app['twilio.sid'] ='SID';
$app['twilio.token'] = 'SecretToken';

$app['notification.sms_sender'] = 'provideNumber';
$app['notification.sms_recipient'] = 'provideNumber';
$app['notification.email_recipient'] = 'provideEmail';
$app['notification.email_sender'] = 'provideEmail';