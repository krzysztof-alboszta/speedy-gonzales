<?php

use Silex\Provider\MonologServiceProvider;

// include the prod configuration
require __DIR__ . '/prod.php';

// enable the debug mode
$app['debug'] = true;
$app['notification_client'] = 'sms.client.dummy';
$app['notification.email_recipient'] = 'krzysztof.alboszta@protonmail.com';
$app['notification.email_sender'] = 'krzysztof.alboszta@protonmail.com';

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__ . '/../var/logs/silex_dev.log',
));