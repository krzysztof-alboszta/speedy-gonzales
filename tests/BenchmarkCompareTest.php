<?php
declare(strict_types=1);

class BenchmarkCompareTest extends \Silex\WebTestCase
{

    public function testCompare()
    {

    }

    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';

        $app['session.test'] = true;

        return $this->app = $app;
    }

    public function createConsole()
    {
        return require __DIR__.'/../src/console.php';
    }
}