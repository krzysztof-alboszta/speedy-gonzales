# Simple benchmark app #

App that performs benchmark of loading time of the website in comparison to the other websites

### Installation ###

Clone repository 
```
git clone git@bitbucket.org:krzysztof-alboszta/speedy-gonzales.git
```

Use composer to install required vendor libraries with 

```
composer install
```

### Usage ###
to run benchmark tool use the CLI console as an entry point, located in `bin/console`

```
bin/console benchmark target-site competitors-sites
```
For detailed help use `--help` option

```
bin/console benchmark --help

Usage:
  benchmark <website-url> <other-urls>

Arguments:
  website-url           Benchmarked website URL
  other-urls            Competitors websites URL [comma separated]

```

`benchmark` command will perform single measurement for the target URL and all competitors URLs as well and summarize the result.

### Outcome
Results are presented on the CLI output and in a `log.txt` file.
The report contains a table ordered by the measured values and additional notes when the target result is not the best one
- if the target result is worse than at least one competitor
- if the target result is at least twice time worse than one of the competitors



### Components ###

#### Internals

* Benchmark service `\Speedy\Service\Benchmark\Benchmark`

Main service to perform test against competitors. It needs to be configured with a desired measurement method
It returns the result object containing all measured values and the position of the target

* Report class `\Speedy\Service\Report\LatencyReport`

Report object related to the Latency measurement method to be used in the presentation layer (Writers)

* Writers 

`\Speedy\Service\Report\Writer\ConsoleWriter` present report in console 

`\Speedy\Service\Report\Writer\FileWriter` save report in a `log.txt` file

 
* Events and Listeners
Results that needs an attention are handled by two listeners to notify via email or sms about such event

`\Speedy\Listener\EmailNotifier`

listening for the `notify.slower` event. It uses `Swift_Mailer` to send email via local mail service

`\Speedy\Listener\SmsNotifier`

listening for the `notify.twice_slower` event. It can be configured to use either Twilio or Dummy sms client

#### Vendors

application is written on the top of below components

* Silex
* Symfony Console
* Guzzlehttp
* twilio
* swiftmailer